---
# Group assignation for hosts:
# - tenant group (necessary, only one)
# - 'unmanaged' group, if provisonned by us but managed by the tenant
# - if not unmanaged:
#   + service groups
#   + specific groups
#
all:
  vars:
    ansible_user: root
    kakunin_os_image: "centos/7"

  children:
    tenant_osci:
      hosts:
        speedy.osci.io:
        guido.osci.io:
        seymour.osci.io:
        jerry.osci.io:
        community-web-builder.int.osci.io:
          ansible_host: 172.24.32.14
        polly.osci.io:
          ansible_host: 8.43.85.229
        francine.osci.io:
          ansible_host: 8.43.85.230
        tickets.osci.io:
          ansible_host: 8.43.85.231
          ansible_python_interpreter: /usr/bin/python3
        osci-web-builder.int.osci.io:
          ansible_host: 172.24.32.11
          ansible_python_interpreter: /usr/bin/python3
        www.osci.io:
          ansible_host: 8.43.85.237
        piwik-vm.osci.io:
          ansible_host: 8.43.85.206
          ansible_python_interpreter: /usr/bin/python3
        soeru.osci.io:
          ansible_host: 8.43.85.211
        lucille.srv.osci.io:
        catton.osci.io:
          ansible_python_interpreter: /usr/bin/python3
        crow-1.int.osci.io:
        crow-2.int.osci.io:
        crow-3.int.osci.io:
        carla.osci.io:
          # don't change DNS settings
          playbook_test_mode: True
        taiste.osci.io:
          ansible_host: 8.43.85.195
        cauldron.osci.io:

    tenant_beaker:
      hosts:
        beaker-project.osci.io:
          ansible_host: 8.43.85.221

    tenant_gdb:
      hosts:
        gdb-buildbot.osci.io:
          ansible_host: 8.43.85.197
        gnutoolchain-gerrit.osci.io:
          ansible_host: 8.43.85.239

    tenant_gnocchi:
      hosts:
        gnocchi.osci.io:
          ansible_host: 8.43.85.232

    tenant_gnome:
      hosts:
        flowbox.gnome.org:

    tenant_jboss:
      hosts:
        lists.jboss.org:
          ansible_host: 8.43.85.220

    tenant_koji:
      hosts:
        www.koji.build:
          ansible_host: 8.43.85.249
        koji-web-builder.int.osci.io:
          ansible_host: 172.24.32.33

    tenant_minishift:
      hosts:
        lists.minishift.io:
          ansible_host: 8.43.85.234

    tenant_nfs_ganesha:
      hosts:
        lists.nfs-ganesha.org:
          ansible_host: 8.43.85.212

    tenant_opendatahub:
      hosts:
        lists.opendatahub.io:
          ansible_host: 8.43.85.215

    tenant_openjdk:
      hosts:
        openjdk-sources.osci.io:
          ansible_host: 8.43.85.238

    tenant_opensourceinfra:
      hosts:
        lists.opensourceinfra.org:
          ansible_host: 8.43.85.233

    tenant_ovirt:
      hosts:
        ovirt-web-builder.int.osci.io:
          ansible_host: 172.24.32.15
        mail.ovirt.org:
          ansible_host: 8.43.85.194
          osci_monitoring: True
        monitoring.ovirt.org:
          ansible_host: 8.43.85.199
        www.ovirt.org:
          ansible_host: 8.43.85.224
        glance.ovirt.org:
          ansible_host: 8.43.85.218

    tenant_pcp:
      hosts:
        pcp.osci.io:
          ansible_host: 8.43.85.210

    tenant_po4a:
      hosts:
        www.po4a.org:
          ansible_host: 8.43.85.209
        lists.po4a.org:
          ansible_host: 8.43.85.228

    tenant_podman:
      hosts:
        lists.podman.io:
          ansible_host: 8.43.85.227

    tenant_pulp:
      hosts:
        www.pulpproject.org:
          ansible_host: 8.43.85.236
        pulp-web-builder.int.osci.io:
          ansible_host: 172.24.32.12
          ansible_python_interpreter: /usr/bin/python3

    tenant_rdo:
      hosts:
        rdo-web-builder.int.osci.io:
          ansible_host: 172.24.32.16

    tenant_scl:
      hosts:
        scl-web.osci.io:
          ansible_host: 8.43.85.196

    tenant_spice:
      hosts:
        spice-web.osci.io:
          ansible_host: 8.43.85.198

    tenant_theopensourceway:
      hosts:
        tosw-ng-2019.theopensourceway.org:
          ansible_host: 8.43.85.213

    tenant_atomic:
      hosts:
        fedora-atomic-1.osci.io:
        fedora-atomic-2.osci.io:
        fedora-atomic-3.osci.io:
        fedora-atomic-4.osci.io:
        fedora-atomic-5.osci.io:

    tenant_patternfly:
      hosts:
        patternfly-forum.osci.io:
          ansible_host: 8.43.85.214
          ansible_python_interpreter: /usr/bin/python3

    tenant_python:
      hosts:
        python-builder-rawhide.osci.io:
          ansible_host: 8.43.85.216
          ansible_python_interpreter: /usr/bin/python3
        python-builder2-rawhide.osci.io:
          ansible_host: 8.43.85.222
          ansible_python_interpreter: /usr/bin/python3
        python-builder-rhel7.osci.io:
          ansible_host: 8.43.85.223
        python-builder-rhel8.osci.io:
          ansible_host: 8.43.85.235
    tenant_kiali:
      hosts:
        kiali-bot.osci.io:
          ansible_host: 8.43.85.217

    tenant_zanata:
      hosts:
        translate.zanata.org:
          ansible_host: 8.43.85.219

    # these hosts are not managed here but needed for some deployments
    # (mostly used for delegations)
    #
    # if osci_monitoring is set to True (on a per tenant/host basis)
    # Zabbix will be deployed on the tenant host and maintained here
    # thus we need root access and be careful for possible conflits
    # TODO: check if part of our base role is needed for deployment
    #       (like enabled package repositories)
    unmanaged:
      vars:
        ansible_user: root
        osci_monitoring: False
      children:
        tenant_atomic:
        tenant_gdb:
        tenant_gnome:
        tenant_ovirt:
        tenant_pcp:
        tenant_pulp:
        tenant_rdo:
        tenant_scl:
        tenant_python:
        tenant_kiali:
        tenant_beaker:
      hosts:
        # listed as unmanaged because ostree wreck havoc with our playbooks
        patternfly-forum.osci.io:

    cage_internal_zone:
      hosts:
        community-web-builder.int.osci.io:
        osci-web-builder.int.osci.io:
        ovirt-web-builder.int.osci.io:
        pulp-web-builder.int.osci.io:
        rdo-web-builder.int.osci.io:
        koji-web-builder.int.osci.io:

    cage_services_zone:
      hosts:
        lucille.srv.osci.io:

    cage_management_zone:
      children:
        supermicro_microblade_sw:

    catatonic:
      children:
        supermicro_microblade_sw:
        supermicro_microblade_blades:
      hosts:
        cmm-catatonic.adm.osci.io:
          ansible_host: 172.24.31.4

    network_equipments:
      children:
        supermicro_microblade_sw:

    not_linux:
      hosts:
        cmm-catatonic.adm.osci.io:
      children:
        supermicro_microblade_sw:

    supermicro_microblade_sw:
      hosts:
        switch-a1-catatonic.adm.osci.io:
          ansible_host: 172.24.31.247
        switch-a2-catatonic.adm.osci.io:
          ansible_host: 172.24.31.248

    supermicro_microblade_blades:
      hosts:
        fedora-atomic-1.osci.io:
        fedora-atomic-2.osci.io:
        fedora-atomic-3.osci.io:
        fedora-atomic-4.osci.io:
        fedora-atomic-5.osci.io:
        cauldron.osci.io:
        jerry.osci.io:
        seymour.osci.io:
        lucille.srv.osci.io:
        catton.osci.io:
        flowbox.gnome.org:
        lb.okd.osci.io:
        master-0.okd.osci.io:
        master-1.okd.osci.io:
        master-2.okd.osci.io:
        worker-0.okd.osci.io:
        worker-1.okd.osci.io:
        worker-2.okd.osci.io:

    virt_hosts:
      hosts:
        speedy.osci.io:
        guido.osci.io:
        jerry.osci.io:
        seymour.osci.io:

    web_builders:
      hosts:
        community-web-builder.int.osci.io:
        osci-web-builder.int.osci.io:

    dns_servers:
      children:
        dns_servers_ns1:
          hosts:
            polly.osci.io:
        dns_servers_ns2:
          hosts:
            francine.osci.io:
            carla.osci.io:

    mail_servers:
      children:
        mail_servers_mx1:
          hosts:
            polly.osci.io:
        mail_servers_mx2:
          hosts:
            francine.osci.io:
            carla.osci.io:

    ml_servers:
      hosts:
        lists.opensourceinfra.org:
        lists.minishift.io:
        lists.po4a.org:
        lists.nfs-ganesha.org:
        lists.opendatahub.io:
        lists.podman.io:
        mail.ovirt.org:
        tosw-ng-2019.theopensourceway.org:

    ntp_servers:
      hosts:
        speedy.osci.io:
        guido.osci.io:

    pxe_servers:
      hosts:
        speedy.osci.io:
        www.osci.io:

    sup_servers:
      hosts:
        catton.osci.io:

    # for these hosts services will NOT be restarted automagically after yum/dnf-cron updates
    # this is intended as a stopgap, proper per-service configuration exceptions are prefered
    needrestart_noauto:
      hosts: {}

