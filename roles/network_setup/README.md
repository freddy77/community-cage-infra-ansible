# Ansible role for Network Configuration

## Introduction

This role setup network configuration on Red Hat systems.

IPv6 is setup if the VLAN has an IPv6 subnet defined. The IPv6 address
is calculated using the subnet CIDR and the link address.

Interfaces are unmanaged by NetworkManager to avoid conflicts, especially NM not behaving properly with binding interfaces.

## Bugs

This role is affected by Ansible#49473, crashing if an IPv6 global address is already defined (because in this case the link-local address is missing from the facts). It is not fixed in 2.7.10, not sure about 2.8.0.

## Settings

TODO: explain VLAN and host settings

